package com.mobicule.spotify.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mobicule.spotify.dao.Factory;
import com.mobicule.spotify.pojo.Music;

public class DeleteMusicController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());

		int Id = Integer.parseInt(request.getParameter("musicId"));

		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();

		Music musicId = session.get(Music.class, Id);
		session.delete(musicId);
		transaction.commit();
		response.sendRedirect("allMusic.jsp");
		session.close();
	}
}
