package com.mobicule.spotify.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mobicule.spotify.dao.Factory;
import com.mobicule.spotify.pojo.Music;

public class UpdateMusicController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String musicName = request.getParameter("musicName");

			int musicId = Integer.parseInt(request.getParameter("musicId").trim());
			Session session = Factory.getFactory().openSession();
			Transaction transaction = session.beginTransaction();

			Music music = session.get(Music.class, musicId);
			music.setMusicName(musicName);
			session.saveOrUpdate(music);

			transaction.commit();
			session.close();

			response.sendRedirect("allMusic.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
