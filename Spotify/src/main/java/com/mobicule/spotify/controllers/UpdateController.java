package com.mobicule.spotify.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mobicule.spotify.dao.Factory;
import com.mobicule.spotify.pojo.Singer;

// UPDATE SINGER
public class UpdateController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String singerName = request.getParameter("singerName");

			int singerId = Integer.parseInt(request.getParameter("singerId").trim());

			Session session = Factory.getFactory().openSession();
			Transaction transaction = session.beginTransaction();

			Singer singer = session.get(Singer.class, singerId);
			singer.setSingerName(singerName);
			session.saveOrUpdate(singer);

			transaction.commit();
			session.close();

			response.sendRedirect("allSingers.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
