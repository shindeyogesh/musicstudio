package com.mobicule.spotify.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.mobicule.spotify.dao.Factory;
import com.mobicule.spotify.pojo.Singer;

// ADD MUSIC
public class MusicController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	Factory factory;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String singerName = request.getParameter("singerName");

			Singer singer = new Singer();
			singer.setSingerName(singerName);
			// System.out.println(singer.getId()+ ":"+ singer.getSingerName());

			Session session = factory.getFactory().openSession();
			Transaction transaction = session.beginTransaction();
			session.save(singer);
			transaction.commit();
			session.close();

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<h1 style='text-align:center;'>Song is added successfully</h1>");
			out.println("<h1 style='text-align:center;'><a href='allSingers.jsp'>View all singers</a></h1>");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
