package com.mobicule.spotify.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobicule.spotify.services.SingerService;

@WebServlet("/singer")
public class SingerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private SingerService service;

	public void init() throws ServletException {
		service = new SingerService();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();
		System.out.println("Action ---> " + action);
		try {
			switch (action) {
			case "/addFormSinger":
				service.addForm(request, response);
				break;
			case "/insertSinger":
				service.insert(request, response);
				break;
			case "/deleteSinger":
				service.delete(request, response);
				break;
			case "/updateFormSinger":
				service.updateForm(request, response);
				break;
			case "/updateSinger":
				service.update(request, response);
				break;
			case "/showAllSinger":
				service.showAll(request, response);
				break;
			default:
				service.showAll(request, response);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}