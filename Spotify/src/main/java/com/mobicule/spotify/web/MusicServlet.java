package com.mobicule.spotify.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobicule.spotify.services.MusicService;

@WebServlet("/music")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
		maxFileSize = 1024 * 1024 * 10, // 10 MB
		maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
public class MusicServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private MusicService service;

	public void init() throws ServletException {
		service = new MusicService();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getServletPath();
		System.out.println("Action ---------------> " + action);
		try {
			switch (action) {
			case "/addFormMusic":
				service.addForm(request, response);
				break;
			case "/insertMusic":
				service.insert(request, response);
				break;
			case "/deleteMusic":
				service.delete(request, response);
				break;
			case "/updateFormMusic":
				service.updateForm(request, response);
				break;
			case "/updateMusic":
				service.update(request, response);
				break;
			case "/showAllMusic":
				service.showAll(request, response);
				break;
			case "/showSingerMusic":
				service.showSingerMusic(request, response);
				break;
			default:
				service.showAll(request, response);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}