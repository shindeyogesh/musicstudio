package com.mobicule.spotify.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.mobicule.spotify.pojo.Singer;

public class SingerDao {

	public void insert(Singer singer) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(singer);
		transaction.commit();
		session.close();
	}

	public Singer selectById(int id) {
		Session session = Factory.getFactory().openSession();
		Singer singer = (Singer) session.get(Singer.class, id);
		return singer;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Singer> selectAll() {
		Session session = Factory.getFactory().openSession();
		Query query = session.createQuery("from Singer");
		List<Singer> singers = query.list();
		return singers;
	}

	public void update(Singer singer) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(singer);
		transaction.commit();
		session.close();
	}

	public void delete(int id) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Singer singer = (Singer) session.get(Singer.class, id);
		System.out.println("Following singer is going to be deleted --> " + singer);
		session.delete(singer);
		transaction.commit();
	}
}
