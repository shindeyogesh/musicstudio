package com.mobicule.spotify.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Service;

@Service
public class Factory {

	public static SessionFactory sessionFactory;

	public static SessionFactory getFactory() {

		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}
		return sessionFactory;
	}

	public void closeFactory() {
		if (sessionFactory.isOpen()) {
			sessionFactory.close();
		}
	}
}
