package com.mobicule.spotify.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.mobicule.spotify.pojo.Music;

@Repository
public class MusicDao {

	public void insert(Music music) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(music);
		transaction.commit();
		session.close();
	}

	public Music selectById(int id) {
		Session session = Factory.getFactory().openSession();
		Music music = (Music) session.get(Music.class, id);
		return music;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Music> selectAll() {
		Session session = Factory.getFactory().openSession();
		Query query = session.createQuery("from Music");
		List<Music> musics = query.list();
		return musics;
	}

	public void update(Music music) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(music);
		transaction.commit();
		session.close();
	}

	public void delete(int id) {
		Session session = Factory.getFactory().openSession();
		Transaction transaction = session.beginTransaction();
		Music music = selectById(id);
		session.delete(music);
		transaction.commit();
	}
}
