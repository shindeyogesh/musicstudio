package com.mobicule.spotify.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.springframework.stereotype.Service;

import com.mobicule.spotify.dao.MusicDao;
import com.mobicule.spotify.dao.SingerDao;
import com.mobicule.spotify.pojo.Music;
import com.mobicule.spotify.pojo.Singer;

@Service
public class MusicService {

	private MusicDao musicDao;
	private SingerDao singerDao;
	private FileService fileService;

	public MusicService() {
		musicDao = new MusicDao();
		singerDao = new SingerDao();
		fileService = new FileService();
	}

	public void addForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Singer> singers = singerDao.selectAll();
		request.setAttribute("singers", singers);
		RequestDispatcher dispatcher = request.getRequestDispatcher("addMusic.jsp");
		dispatcher.forward(request, response);
	}

	public void insert(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		Part musicFile = request.getPart("file");
		String musicName = musicFile.getSubmittedFileName();
		System.out.println("Music Name -----------> " + musicName);

		String dbMusicName = fileService.uploadMusic("C:\\Users\\91960\\OneDrive\\Desktop\\Music", musicFile);
		System.out.println("DB Music Name --------> " + dbMusicName);

		int singerId = Integer.parseInt(request.getParameter("singerId"));
		Singer singer = singerDao.selectById(singerId);
		System.out.println("Choosen Singer Name --> " + singer.getSingerName());

		Music music = new Music();
		music.setMusicName(musicName);
		music.setStoredFileName(dbMusicName);
		music.setSinger(singer);
		musicDao.insert(music);
		System.out.println("<-- Music  Inserted --> " + music);
		response.sendRedirect("showAllMusic");
	}

	public void delete(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		int musicId = Integer.parseInt(request.getParameter("musicId"));
		try {
			musicDao.delete(musicId);
			System.out.println("<--- Music Deleted ---> " + musicId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("showAllMusic");
	}

	public void updateForm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		int musicId = Integer.parseInt(request.getParameter("musicId"));
		Music music;
		try {
			music = musicDao.selectById(musicId);
			System.out.println("Music Found ----------> " + music);
			RequestDispatcher dispatcher = request.getRequestDispatcher("updateMusic.jsp");
			request.setAttribute("music", music);
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		System.out.println("Music Id -------------> " + request.getParameter("musicId"));
		int id = Integer.parseInt(request.getParameter("musicId"));
		String musicName = request.getParameter("musicName");
		System.out.println("New Music Name -------> " + musicName);
		Music music = new Music();
		music.setId(id);
		music.setMusicName(musicName);
		musicDao.update(music);
		System.out.println("<--- Music Updated ---> " + music);
		response.sendRedirect("showAllMusic");
	}

	public void showAll(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		try {
			List<Music> musics = musicDao.selectAll();
			System.out.println("Musics ---------------> " + musics);
			request.setAttribute("musics", musics);
			RequestDispatcher dispatcher = request.getRequestDispatcher("allMusic.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showSingerMusic(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		try {
			int singerId = Integer.parseInt(request.getParameter("singerId"));
			Singer singer = singerDao.selectById(singerId);
			List<Music> musics = singer.getMusics();
			System.out.println("Musics ---------------> " + musics);
			request.setAttribute("musics", musics);
			RequestDispatcher dispatcher = request.getRequestDispatcher("allMusic.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
