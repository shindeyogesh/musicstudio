package com.mobicule.spotify.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.http.Part;

import org.springframework.stereotype.Service;

@Service
public class FileService {

	public String uploadMusic(String path, Part filePart) throws IOException {

		// Random Name Generate File
		String randomId = UUID.randomUUID().toString();
		String randomFileName = randomId.concat(".mp3");

		// Full Path
		String filePath = path + File.separator + randomFileName;

		// Create Folder If Not Created
		File f = new File(path);
		if (!f.exists())
			f.mkdir();

		filePart.write(filePath + randomFileName);

		return randomId;
	}

	public InputStream getResource(String path, String fileName) throws FileNotFoundException {
		String fullPath = path + File.separator + fileName;
		InputStream inputStream = new FileInputStream(fullPath);
		return inputStream;
	}
}
