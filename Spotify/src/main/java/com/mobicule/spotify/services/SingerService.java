package com.mobicule.spotify.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobicule.spotify.dao.SingerDao;
import com.mobicule.spotify.pojo.Singer;

public class SingerService {

	private SingerDao singerDao;

	public SingerService() {
		singerDao = new SingerDao();
	}

	public void addForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("addSinger.jsp");
		dispatcher.forward(request, response);
	}

	public void insert(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String singerName = request.getParameter("singerName");
		Singer singer = new Singer();
		singer.setSingerName(singerName);
		singerDao.insert(singer);
		response.sendRedirect("showAllSinger");
	}

	public void delete(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		System.out.println("Inside Delete Singer");
		int id = Integer.parseInt(request.getParameter("singerId"));
		try {
			singerDao.delete(id);
			System.out.println("Singer Deleted Successfully" + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("showAllSinger");
	}

	public void updateForm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		int id = Integer.parseInt(request.getParameter("singerId"));
		Singer singer;
		try {
			singer = singerDao.selectById(id);
			RequestDispatcher dispatcher = request.getRequestDispatcher("updateSinger.jsp");
			request.setAttribute("singer", singer);
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		int id = Integer.parseInt(request.getParameter("singerId"));
		String singerName = request.getParameter("singerName");
		Singer singer = new Singer();
		singer.setId(id);
		singer.setSingerName(singerName);
		singerDao.update(singer);
		response.sendRedirect("showAllSinger");
	}

	public void showAll(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		try {
			List<Singer> singers = singerDao.selectAll();
			request.setAttribute("singers", singers);
			RequestDispatcher dispatcher = request.getRequestDispatcher("allSinger.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
