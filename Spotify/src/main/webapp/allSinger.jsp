<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.mobicule.spotify.dao.Factory"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mobicule.spotify.*"%>
<%@page import="com.mobicule.spotify.pojo.Singer"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>All Singers</title>
<%@include file="all_js_css.jsp"%>

<div class="container">
	<%@include file="navbar.jsp"%>
	<br>
	<h1 class="text-uppercase">All Singers</h1>
	<div class="row">
		<div class="col-12">
			<%
				List<Singer> singers = (ArrayList<Singer>) request.getAttribute("singers");
				for (Singer singer : singers) {
			%>
			<div class="card mt-0">
				<img class="card-img-top m-4 mx-auto mt-2" src="img/music.png"
					style="max-width: 100px;" alt="Card image cap">
				<div class="card-body px-5">
					<h5 class="card-title"><%=singer.getSingerName()%></h5>
					<p class="card-text">
					<div class="container text-center">
						<a href="deleteSinger?singerId=<%=singer.getId()%>"
							class="btn btn-danger">Delete</a> 
						<a href="updateFormSinger?singerId=<%=singer.getId()%>"
							class="btn btn-primary">Update</a>
						<a href="showSingerMusic?singerId=<%=singer.getId()%>"
							class="btn btn-warning">Music</a>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
	</div>
</div>
</head>
</html>