<%@page import="java.util.ArrayList"%>
<%@page import="com.mobicule.spotify.pojo.Music"%>
<%@page import="com.mobicule.spotify.dao.Factory"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="com.mobicule.spotify.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>All Music</title>
<%@include file="all_js_css.jsp"%>

<div class="container">
	<%@include file="navbar.jsp"%>
	<br>
	<h1 class="text-uppercase">All music</h1>
	<div class="row">
		<div class="col-12">
			<%
				List<Music> musics = (List<Music>) request.getAttribute("musics");
				for (Music music : musics) {
			%>
			<div class="card mt-0">
				<img class="card-img-top m-4 mx-auto mt-2" src="img/music.png"
					style="max-width: 100px;" alt="Card image cap">
				<div class="card-body px-5">
					<h5 class="card-title"><%=music.getMusicName()%></h5>
					<p class="card-text">
					<div class="container text-center">
						<a href="deleteMusic?musicId=<%=music.getId()%>"
							class="btn btn-danger">Delete</a> 
						<a href="updateFormMusic?musicId=<%=music.getId()%>"
							class="btn btn-primary">Update</a>
					</div>
				</div>
			</div>
			<%
				}
			%>
		</div>
	</div>
</div>
</head>
</html>