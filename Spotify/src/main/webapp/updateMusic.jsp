<%@page import="org.hibernate.Session"%>
<%@page import="com.mobicule.spotify.pojo.Music"%>
<%@page import="com.mobicule.spotify.dao.Factory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.mobicule.spotify.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UpdateMusic</title>

<%@include file="all_js_css.jsp"%>
</head>
<body>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<br>
		<h2>Music Update Page</h2>
		<br>
		<%
			Music music = (Music) request.getAttribute("music");
		%>
		<form action="updateMusic" method="post">
			<div class="form-group">
				<input value="<%=music.getId()%>" name="musicId" type="hidden" /> <label
					for="music">Update Music</label> <input name="musicName"
					type="text" class="form-control" id="musicName"
					placeholder="<%=music.getMusicName()%>" required />
			</div>
			<div class="container text-center">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
	</div>
</body>
</html>