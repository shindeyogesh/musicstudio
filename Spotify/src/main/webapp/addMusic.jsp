<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mobicule.spotify.pojo.Singer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Music</title>
<%@include file="all_js_css.jsp"%>
</head>
<body>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<br>
		<h2>Add Music page</h2>
		<br>

		<%
		List<Singer> singers = (ArrayList<Singer>) request.getAttribute("singers");
		%>
		<form action="insertMusic" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<input type="file" name="file" required/> 
				<br><br>
				<label>Choose Singer Of The Music -->    </label>
				<select name="singerId">
					<%
					for (Singer singer : singers) {
					%>
					<option value="<%=singer.getId()%>"><%=singer.getSingerName()%></option>
					<%
					}
					%>
				</select>
				<br>
			</div>
			<div class="container text-center">
				<button type="submit" class="btn btn-primary">Add</button>
			</div>
		</form>
	</div>
</body>
</html>