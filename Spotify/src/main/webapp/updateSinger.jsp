<%@page import="com.mobicule.spotify.pojo.Singer"%>
<%@page import="com.mobicule.spotify.dao.Factory"%>
<%@page import="org.hibernate.Session"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.mobicule.spotify.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Singer Update Page</title>

<%@include file="all_js_css.jsp"%>
</head>
<body>
	<div class="container">
		<%@include file="navbar.jsp"%>
		<br>
		<h2>This is Update Page</h2>
		<br>
		<%
			int id = Integer.parseInt(request.getParameter("singerId").trim());
			Session sesssion = Factory.getFactory().openSession();
			Singer singer = (Singer) sesssion.get(Singer.class, id);
		%>
		<form action="updateSinger" method="post">
			<input value="<%=singer.getId()%>" name="singerId" type="hidden" />

			<div class="form-group">
				<label for="singer">Edit Singer</label> <input name="singerName"
					required type="text" class="form-control" id="singerName"
					aria-describedby="Singer" placeholder="Enter Singer Name"
					value="<%=singer.getSingerName()%>" />
			</div>

			<div class="container text-center">
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</form>
	</div>
</body>
</html>