<%@include file="all_js_css.jsp"%>
<nav class="navbar navbar-expand-lg navbar-dark purple">
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="index.jsp">Home<span class="sr-only">(current)</span> </a>
			</li>
			
			<li class="nav-item">
				<a class="nav-link" href="addFormSinger">Add Singer</a>
			</li>
			
			<li class="nav-item dropdown"></li>
			<li class="nav-item">
				<a class="nav-link disabled" href="showAllSinger">Show Singer</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="addFormMusic">Add Music</a>
			</li>
			
			<li class="nav-item dropdown"></li>
			<li class="nav-item">
				<a class="nav-link disabled" href="showAllMusic">Show Music</a>
			</li>
		</ul>

		<form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="search"
				placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form>
	</div>
</nav>